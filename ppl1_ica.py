import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy import linalg

# Krok 0 - Storzenie m liniowych mieszanin (v) dla n niezależnych składowych (s) gdzie m>=n ; tutaj m=n=3
A = np.array([[0.5, 0.8, 0.3], 
              [0.8, 0.4, 0.6],
              [0.4, 0.2, 0.9]])

t = np.linspace(0, 1, 100)
s1 = np.sin(2*np.pi*t)
s2 = signal.sawtooth(2 * np.pi * 4 * t, 0.3)
s3 = signal.square(2 * np.pi * 2 * t)


v1 = A[0][0]*s1+A[0][1]*s2+A[0][2]*s3
v2 = A[1][0]*s1+A[1][1]*s2+A[1][2]*s3
v3 = A[2][0]*s1+A[2][1]*s2+A[2][2]*s3

#Krok 1 - Wycentrowanie mieszanin poprzez odjęcie od nich ich wartości średnich

vc1 = v1 - (np.ones(len(v1)) * np.mean(v1))
vc2 = v2 - (np.ones(len(v2)) * np.mean(v2))
vc3 = v3 - (np.ones(len(v3)) * np.mean(v3))
vc = np.array([vc1,vc2,vc3])

#Krok 2 - Wybielenie mieszanin - liniowa modyfikacja wektorów v (z wykorzystaniem macierzy M)
# w celu uzyskania wektorów x pozbawionych związków korelacyjnych między elementami

xcov = np.cov(vc, bias=True)

# v to wartości własne (Eigenvalues) ; w to wektory własne (Eigenvectors)
v, w = linalg.eig(xcov)

# M = w * diagv ** (-1/2) * w.T <transponowane>
diagv = np.diag(1/(v**0.5)).real
M = np.dot(np.dot(w,diagv), w.T)
x = np.dot(M, vc)
#print(np.cov(x))

B = np.array([])

for n in range(x.shape[0]):
    
    #Krok 3 - Inicjalizacja losowego wektora W o normie = 1
    Wstare = np.random.rand(x.shape[0],1)
   
    # Krok Bonusowy v1 - Ponieważ rozdzielamy więcej niż 2 składowe niezależne, to należy się upewnić że za każdym razem wydobędziemy inną składową
    if n>0:
        Wstare -= np.dot(np.dot(B,B.T),Wstare)
        
    #Krok 3 cd
    Wstare /= np.linalg.norm(Wstare)*np.ones((Wstare.shape))

    #Krok 4 - Iteracyjna ekstrakcja jednego źródła (powtarzana aż wynik mnożenia Wnowe*Wstare będzie dostatnie bliski wartości 1)
    eps = 1
    while (eps > 0.00001):
        xwx3 = x * np.power(np.dot(Wstare.T,x),3)
        Wnowe = -3*Wstare
        for i in range (len(Wnowe)):
            Wnowe[i] += np.mean(xwx3[i])
        # Krok Bonusowy v2 - Ponieważ rozdzielamy więcej niż 2 składowe niezależne, to należy się upewnić że za każdym razem wydobędziemy inną składową
        if n>0:
            Wnowe -= np.dot(np.dot(B,B.T),Wnowe)
        # Krok 5 - Normalizacja Wnowe (czyli w[i] bo Wstare to w[i-1]) poprzez podzielenie go przez jego normę
        Wnowe/= np.linalg.norm(Wnowe)*np.ones((Wnowe.shape))
        eps = 1 - abs(np.dot(Wnowe.T,Wstare))
        Wstare = Wnowe
    if n==0:
        B = Wnowe.copy()
    else:
        B = np.column_stack((B,Wnowe))
  
        
# # Ten krok ma sens tylko przy rozdzielaniu 2 składowych niezależnych, wówczas nie stosujemy powyżej pętli for
# # Krok 6 - Definicja unormowanej macierzy V ortogonalnej do Wnowe
# V =  linalg.null_space(Wnowe.T)
# V/= np.linalg.norm(V)*np.ones((V.shape))
# # print(np.dot(Wnowe.T,V))
# B = np.column_stack((Wnowe,V))

# Krok ostatni - mnożymy ortogonalną macierz B przez wybielone i wycentrowane mieszaniny
Rezultat = np.dot(B.T,x)
r1=Rezultat[0]
r2=Rezultat[1]
r3=Rezultat[2]

# Wykresy
fig, axs = plt.subplots(3, 3, figsize=(12, 8))
axs[0, 0].plot(t, s1)
axs[0, 0].set_title('Sygnał 1')
axs[0, 1].plot(t, s2)
axs[0, 1].set_title('Sygnał 2')
axs[0, 2].plot(t, s3)
axs[0, 2].set_title('Sygnał 3')
axs[1, 0].plot(t, v1)
axs[1, 0].set_title('Mix 1')
axs[1, 1].plot(t, v2)
axs[1, 1].set_title('Mix 2')
axs[1, 2].plot(t, v3)
axs[1, 2].set_title('Mix 3')
axs[2, 0].plot(t, r1)
axs[2, 0].set_title('Odzyskany Sygnał 1')
axs[2, 1].plot(t, r2)
axs[2, 1].set_title('Odzyskany Sygnał 2')
axs[2, 2].plot(t, r3)
axs[2, 2].set_title('Odzyskany Sygnał 3')
axs[2, 2].set
plt.tight_layout()
plt.show()