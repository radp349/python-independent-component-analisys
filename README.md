# README

## Opis projektu

Ten skrypt Pythona jest przykładem implementacji algorytmu FastICA (Independent Component Analysis) w celu rozdzielania sygnałów źródłowych, które zostały wymieszane. FastICA to algorytm służący do odzyskiwania niezależnych składowych sygnału. 

## Wymagania

Aby uruchomić ten skrypt, potrzebne są następujące biblioteki:
- numpy
- matplotlib
- scipy

## Instrukcje

Po zainstalowaniu wymaganych bibliotek, możesz uruchomić skrypt. Skrypt nie wymaga żadnych argumentów wejściowych. 

Skrypt rozpoczyna się od utworzenia trzech sygnałów źródłowych (sinusoidalnego, piłokształtnego i kwadratowego) oraz ich mieszania za pomocą określonej macierzy mieszającej. Następnie sygnały mieszane są centrowane poprzez odjęcie wartości średniej, a następnie są "wybielane", co oznacza, że są przekształcane w taki sposób, aby stały się nieskorelowane i miały jednostkową wariancję.

Następnie skrypt inicjalizuje losowy wektor wagi, a następnie iteracyjnie go aktualizuje, aby ekstrahować jedno źródło. To jest robione dla każdej niezależnej składowej. 

W końcu skrypt mnoży macierz wag przez zbiór sygnałów mieszanych, aby odzyskać sygnały źródłowe, a następnie wyświetla zarówno sygnały mieszane, jak i odzyskane na wykresach.

## Wizualizacja

Skrypt kończy się tworzeniem wykresów dla każdego z oryginalnych sygnałów, ich mieszanin oraz odzyskanych sygnałów. Można to wykorzystać do wizualnej oceny skuteczności algorytmu FastICA.
